package com.rave.characterviewer.data.remote.dto


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class CharacterDTO(
    @SerialName("FirstURL")
    val firstURL: String?,
    @SerialName("Icon")
    val icon: IconDTO?,
    @SerialName("Result")
    val result: String?,
    @SerialName("Text")
    val text: String?
)