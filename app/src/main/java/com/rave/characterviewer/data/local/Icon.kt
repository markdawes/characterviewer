package com.rave.characterviewer.data.local

data class Icon(
    val height: String?,
    val uRL: String?,
    val width: String?
)