package com.rave.characterviewer.data.local

data class Character(
    val image: String,
    val title: String,
    val description: String
)