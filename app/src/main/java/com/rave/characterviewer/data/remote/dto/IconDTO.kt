package com.rave.characterviewer.data.remote.dto


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class IconDTO(
    @SerialName("Height")
    val height: String?,
    @SerialName("URL")
    val uRL: String?,
    @SerialName("Width")
    val width: String?
)