package com.rave.characterviewer.data.remote.dto


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class CharacterResponse(
    @SerialName("RelatedTopics")
    val relatedTopics: List<CharacterDTO>
)