package com.rave.characterviewer.data.remote

import com.rave.characterviewer.BuildConfig
import com.rave.characterviewer.data.remote.dto.CharacterResponse
import retrofit2.http.GET

/**
 * Service class that creates the endpoints needed.
 *
 * @constructor Create empty Api service
 */
interface APIService {

    @GET(ENDPOINT)
    suspend fun getCharacters(): CharacterResponse

    companion object {
        private const val ENDPOINT = BuildConfig.API_URL
    }
}