package com.rave.characterviewer.data

import com.rave.characterviewer.data.remote.APIService
import com.rave.characterviewer.data.local.Character
import com.rave.characterviewer.data.remote.dto.CharacterDTO
import java.net.URLDecoder
import javax.inject.Inject
import org.xml.sax.Parser

class CharacterRepo @Inject constructor(private val service: APIService){

    suspend fun getCharacters(): List<Character> {
        val characterDTOs = service.getCharacters().relatedTopics
        return characterDTOs.map {
            Character(
                image = parseImage(it),
                title = parseTitle(it.firstURL!!),
                description = it.text!!
            )
        }
    }

    private fun parseTitle(url: String): String {
        val baseUrl = "https://duckduckgo.com/"
        if (!url.startsWith(baseUrl)) {
            throw IllegalArgumentException("Invalid URL: $url")
        }
        return url.substring(baseUrl.length)
            .replace('_', ' ')
            .let { URLDecoder.decode(it, "UTF-8") }
    }

    private fun parseImage(character: CharacterDTO): String {
        return if (character.icon?.uRL.isNullOrBlank()) {
            "no image"
        } else {
            character.firstURL + character.icon?.uRL
        }
    }
}