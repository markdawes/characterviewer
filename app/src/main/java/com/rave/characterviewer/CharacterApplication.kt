package com.rave.characterviewer

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Custom application class for hilt.
 *
 * @constructor Create empty Character application
 */
@HiltAndroidApp
class CharacterApplication : Application()