package com.rave.characterviewer.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.characterviewer.data.CharacterRepo
import com.rave.characterviewer.data.local.Character
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.launch

@HiltViewModel
class CharacterListViewModel @Inject constructor(private val repo: CharacterRepo) : ViewModel(){
    private var _characters: MutableLiveData<List<Character>> = MutableLiveData()
    val characters: LiveData<List<Character>> get() = _characters

    init {
        fetchCharacters()
    }

    private fun fetchCharacters() = viewModelScope.launch {
        _characters.value = repo.getCharacters()
    }
}