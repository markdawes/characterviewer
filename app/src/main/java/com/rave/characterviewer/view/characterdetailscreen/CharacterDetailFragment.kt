package com.rave.characterviewer.view.characterdetailscreen

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import coil.load
import com.rave.characterviewer.databinding.FragmentCharacterDetailBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CharacterDetailFragment : Fragment() {

    private var _binding: FragmentCharacterDetailBinding? = null
    private val binding get() = _binding!!

    private val title by lazy { arguments?.getString("title") }
    private val image by lazy { arguments?.getString("image") }
    private val description by lazy { arguments?.getString("description") }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return FragmentCharacterDetailBinding.inflate(inflater, container, false).apply {
            _binding = this
            binding.characterTitle.text = title
            Log.i("MYTAG", "image is $image")
            if(image != "no image") {
                binding.characterImage.load(image)
            }
            binding.characterDescription.text = description
        }.root
    }
}