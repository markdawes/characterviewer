package com.rave.characterviewer.view.characterlistscreen

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.rave.characterviewer.data.local.Character
import androidx.recyclerview.widget.LinearLayoutManager
import com.rave.characterviewer.databinding.FragmentCharacterListBinding
import com.rave.characterviewer.viewmodel.CharacterListViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CharacterListFragment : Fragment() {

    private var _binding: FragmentCharacterListBinding? = null
    private val binding get() = _binding!!

    private val viewModel by viewModels<CharacterListViewModel>()

    private lateinit var characterAdapter: CharacterAdapter
    private var characters: List<Character> = listOf()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return FragmentCharacterListBinding.inflate(inflater, container, false).apply {
            _binding = this
            characterAdapter = CharacterAdapter()
            viewModel.characters.observe(viewLifecycleOwner) { fetchedCharacters ->
                characters = fetchedCharacters
                characterAdapter.setData(characters)
            }
            binding.rvCharacters.adapter = characterAdapter
            binding.rvCharacters.layoutManager = LinearLayoutManager(requireContext())

            // Add the TextWatcher to the search EditText
            binding.etSearch.addTextChangedListener(searchTextWatcher)
        }.root
    }

    private val searchTextWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            filterCharacters(s.toString())
        }

        override fun afterTextChanged(s: Editable) {}
    }

    private fun filterCharacters(query: String) {
        val filteredCharacters = characters.filter { character ->
            character.description.contains(query, ignoreCase = true)
        }
        characterAdapter.setData(filteredCharacters)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.etSearch.removeTextChangedListener(searchTextWatcher)
        _binding = null
    }
}