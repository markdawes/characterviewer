package com.rave.characterviewer.view.characterlistscreen

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.rave.characterviewer.R
import com.rave.characterviewer.data.local.Character
import com.rave.characterviewer.databinding.ItemCharacterBinding

class CharacterAdapter : RecyclerView.Adapter<CharacterAdapter.CharacterViewHolder>() {

    private var characterList = emptyList<Character>()

    inner class CharacterViewHolder(private val binding: ItemCharacterBinding) : RecyclerView.ViewHolder(binding.root) {

        fun displayCharacter(character: Character) = with(binding) {
            tvCharacter.text = character.title
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return CharacterViewHolder(
            ItemCharacterBinding.inflate(inflater, parent, false)
        )
    }

    override fun onBindViewHolder(holder: CharacterViewHolder, position: Int) {
        val currentItem = characterList[position]
        holder.displayCharacter(currentItem)
        holder.itemView.rootView.setOnClickListener {
            val args = bundleOf(
                "title" to currentItem.title,
                "image" to currentItem.image,
                "description" to currentItem.description
            )
            Navigation.findNavController(holder.itemView).navigate(
                R.id.action_characterListFragment_to_characterDetailFragment,
                args
            )
        }
    }

    override fun getItemCount(): Int {
        return characterList.size
    }

    fun setData(character: List<Character>) {
        this.characterList = character
        notifyDataSetChanged()
    }
}