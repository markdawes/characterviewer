Single Codebase Character Viewer App

The app is divided into the following main components:

app module: Contains the shared code for both variants of the app.

Simpsons Character Viewer flavor: Configures the app for displaying Simpsons characters.

The Wire Character Viewer flavor: Configures the app for displaying Wire characters.

Product Flavors Configuration

In the app's build.gradle file, I have configured product flavors to create two variants of the app:
Here, I define a flavor dimension named api and create two product flavors: simpsons and wire. Each flavor has a unique applicationId and a different API_URL to fetch character data.

We access the API_URL defined in the product flavors using BuildConfig.API_URL in our APIService. This way, the correct API URL is used depending on the chosen flavor.

To build and run the app for a specific flavor, select the desired flavor from the "Build Variants" panel in Android Studio. Then, build and run the app as usual. The chosen flavor will use the appropriate API URL and display either Simpsons or Wire characters.

You can also build and run the app for both flavors using the command line:
./gradlew installSimpsonsDebug
./gradlew installWireDebug

These commands will build and install the debug versions of the simpsons and wire flavors on the connected device or emulator.